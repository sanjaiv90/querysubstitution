package metadata;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;

public class MetaDataOfQueryTest {
	public static void main(String[] args) {
		
		System.out.println("Test Connectivity to MariaDB");
		Connection con = null;
		String url = "jdbc:mysql://localhost:3306/";
		String db = "mysql";
		String driver = "org.svayam.jdbc.SvayamDriver";
		
		try {
			Class.forName(driver);
			con = DriverManager.getConnection(url+db, "root", "fun2learn");
			Statement st = con.createStatement();
			String sql = "select * from user";
			ResultSet rs = st.executeQuery(sql);
			String tablename=rs.getMetaData().getTableName(1);
			 System.out.println(tablename);
			ResultSetMetaData rsmd = rs.getMetaData();
			int columnsNumber = rsmd.getColumnCount();
			while (rs.next()) {
			   for (int i = 1; i <= columnsNumber; i++) {
			       if (i > 1) System.out.print(",  ");
			          String columnValue = rs.getString(i);
			          System.out.print(columnValue + " " + rsmd.getColumnName(i));
			       }
			       System.out.println("");
			   }
		} catch (Exception e){
			System.out.println(e);
		}

	}

}
