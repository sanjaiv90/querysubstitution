package metadata;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ConnectToSvayamDev {

	public static List<String>  getConfigDbMatadata(){
		Connection con = null;
		String url = "jdbc:mysql://localhost:3306/";
		String db = "svayam_dev";
		String driver = "org.mariadb.jdbc.Driver";
		List<String> databaseList=new ArrayList<String>();
		try {
			Class.forName(driver);
			con = DriverManager.getConnection(url+db, "root", "fun2learn");
			Statement st = con.createStatement();
			String sql = "select * from configDb";
			ResultSet rs = st.executeQuery(sql);
		
			while (rs.next()) {
				databaseList.add(rs.getString("dbName"));
			  }
			con.close();
		} catch (Exception e){
			System.out.println(e);
		}
		
		return databaseList;
	}
}
