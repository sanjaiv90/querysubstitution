package metadata;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;

import org.mariadb.jdbc.Driver;

public class MetadataColumnTest{
	public static void main(String[] args) {
	Connection con = null;
	String url = "jdbc:mysql://localhost:3306/";
	String db = "svayam_dev";
	String driver = "org.svayam.jdbc.SvayamDriver";
	try {
		Class.forName(driver);
		con = DriverManager.getConnection(url+db, "root", "fun2learn");
		DatabaseMetaData md = con.getMetaData();
		
	    ResultSet r = md.getColumns("", "", "transaction_bal", "%");
	    int i=r.getMetaData().getColumnCount();
	      
	    System.out.println(i);
	    while (r.next())
	        System.out.println("\t" + r.getString(4) + " : "+ r.getString(5) + " : "
	                + r.getString(6)+" : "+ r.getString(24) + " : ");
	 	
		
		/*Statement st = con.createStatement();
		String sql = "select * from configDb";
		ResultSet rs = st.executeQuery(sql);
		ResultSetMetaData rsmd = rs.getMetaData();
		
		
		 int columnCount = rsmd.getColumnCount();
		 for (int i = 1; i <= columnCount; i++ ) {
			  System.out.println( rsmd.getColumnName(i)+","+rsmd.getTableName(i));
			  // Do stuff with name
			}*/
		con.close();
	} catch (Exception e){
		System.out.println(e);
	}
 }
}
