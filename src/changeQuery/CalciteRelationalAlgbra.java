package changeQuery;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import javax.sql.DataSource;

import org.apache.calcite.adapter.enumerable.EnumerableTableScan;
import org.apache.calcite.adapter.java.JavaTypeFactory;
import org.apache.calcite.adapter.jdbc.JdbcSchema;
import org.apache.calcite.config.Lex;
import org.apache.calcite.jdbc.CalciteConnection;
import org.apache.calcite.jdbc.CalcitePrepare;
import org.apache.calcite.plan.Convention;
import org.apache.calcite.plan.RelOptAbstractTable;
import org.apache.calcite.plan.RelOptCluster;
import org.apache.calcite.plan.RelOptPlanner;
import org.apache.calcite.plan.RelOptUtil;
import org.apache.calcite.plan.RelTrait;
import org.apache.calcite.plan.RelTraitDef;
import org.apache.calcite.plan.SubstitutionVisitor;
import org.apache.calcite.plan.volcano.VolcanoPlanner;
import org.apache.calcite.prepare.CalciteCatalogReader;
import org.apache.calcite.prepare.PlannerImpl;
import org.apache.calcite.rel.AbstractRelNode;
import org.apache.calcite.rel.RelNode;
import org.apache.calcite.rel.RelRoot;
import org.apache.calcite.rel.RelShuttle;
import org.apache.calcite.rel.core.AggregateCall;
import org.apache.calcite.rel.core.Filter;
import org.apache.calcite.rel.core.JoinRelType;
import org.apache.calcite.rel.core.Project;
import org.apache.calcite.rel.logical.LogicalAggregate;
import org.apache.calcite.rel.logical.LogicalProject;
import org.apache.calcite.rel.logical.LogicalUnion;
import org.apache.calcite.rel.type.RelDataType;
import org.apache.calcite.rel.type.RelDataTypeFactory;
import org.apache.calcite.rel.type.RelDataTypeField;
import org.apache.calcite.rel.type.RelDataTypeFieldImpl;
import org.apache.calcite.rex.RexBuilder;
import org.apache.calcite.rex.RexCall;
import org.apache.calcite.rex.RexNode;
import org.apache.calcite.schema.SchemaPlus;
import org.apache.calcite.schema.Table;
import org.apache.calcite.schema.impl.AbstractTable;
import org.apache.calcite.server.CalciteServerStatement;
import org.apache.calcite.sql.SqlCall;
import org.apache.calcite.sql.SqlNode;
import org.apache.calcite.sql.SqlOperator;
import org.apache.calcite.sql.SqlOperatorTable;
import org.apache.calcite.sql.SqlSelect;
import org.apache.calcite.sql.fun.SqlStdOperatorTable;
import org.apache.calcite.sql.parser.SqlParseException;
import org.apache.calcite.sql.parser.SqlParser;
import org.apache.calcite.sql.type.SqlTypeName;
import org.apache.calcite.sql.validate.SqlConformance;
import org.apache.calcite.sql.validate.SqlConformanceEnum;
import org.apache.calcite.sql.validate.SqlValidator;
import org.apache.calcite.sql.validate.SqlValidatorCatalogReader;
import org.apache.calcite.sql.validate.SqlValidatorImpl;
import org.apache.calcite.sql.validate.SqlValidatorUtil;
import org.apache.calcite.sql.validate.SqlValidatorWithHints;
import org.apache.calcite.sql2rel.ReflectiveConvertletTable;
import org.apache.calcite.sql2rel.SqlNodeToRexConverter;
import org.apache.calcite.sql2rel.SqlRexContext;
import org.apache.calcite.sql2rel.SqlRexConvertlet;
import org.apache.calcite.sql2rel.SqlToRelConverter;

import org.apache.calcite.tools.FrameworkConfig;
import org.apache.calcite.tools.Frameworks;
import org.apache.calcite.tools.Planner;
import org.apache.calcite.tools.Programs;
import org.apache.calcite.tools.RelBuilder;
import org.apache.calcite.tools.RelBuilder.AggCall;
import org.apache.calcite.util.ImmutableBitSet;
import org.apache.calcite.tools.RelConversionException;
import org.apache.calcite.tools.RelRunners;
import org.apache.calcite.tools.ValidationException;
import org.apache.commons.codec.language.bm.Rule.RPattern;

import com.google.common.collect.ImmutableList;

public class CalciteRelationalAlgbra extends SqlValidatorImpl  {
	
	public CalciteRelationalAlgbra(SqlOperatorTable opTab, SqlValidatorCatalogReader catalogReader,
			RelDataTypeFactory typeFactory, SqlConformance conformance) {
		super(opTab, catalogReader, typeFactory, conformance);
		// TODO Auto-generated constructor stub
	}



	@SuppressWarnings("deprecation")
	public static void main(String[] args) throws SQLException, SqlParseException, ValidationException, RelConversionException {
		
		 String url="jdbc:mysql://localhost:3306/SVAYAM_DEV";
		 String driverClassName="org.mariadb.jdbc.Driver";
		 String username="root";
		 String password="fun2learn";
		 Connection calCon=DriverManager.getConnection("jdbc:calcite:");
		 CalciteConnection calciteConnection=calCon.unwrap(CalciteConnection.class);
		 SchemaPlus rootSchema=calciteConnection.getRootSchema();
		 final DataSource dataSource=JdbcSchema.dataSource(url, driverClassName, username, password);
		final FrameworkConfig config =
		        Frameworks.newConfigBuilder()
		                .parserConfig(SqlParser.configBuilder().setLex(Lex.MYSQL).build())
		                .defaultSchema(rootSchema.add("test",
		                        JdbcSchema.create(rootSchema, "svayam_dev",
		dataSource, null, null)))
		                .traitDefs((List<RelTraitDef>) null)

		.programs(Programs.heuristicJoinOrder(Programs.RULE_SET, true, 2))
		                .build();
		
		
		
		final RelBuilder builder = RelBuilder.create(config);
		
		RelNode customer =
		        builder.scan("customer").scan("transaction")
		         .join(JoinRelType.INNER, "cust_id")
		        .project(builder.field("cust_id"),builder.field("txn_amt"))
		                .filter(
		                        builder.equals(builder.field("cust_id"),
		builder.literal(10)))
		                .build();
		
		
		RelNode transaction =
		        builder.scan("transaction")
	            	        
		        .project(builder.field("cust_id"), builder.field("txn_amt"))
		        
		        /*  .filter(
		                        builder.equals(builder.field("cust_id"),
		builder.literal(10)) )*/
		                .build();
		
		
		RelNode union=LogicalUnion.create(ImmutableList.of(customer, transaction), true);
		
		System.out.println(RelOptUtil.toString(union.getInput(1)));
		
		RelNode join=builder.push(customer).push(transaction)
				     .join(JoinRelType.INNER, "cust_id")
				.build();
		RelNode absRelNode=null ;
 		Planner planner = Frameworks.getPlanner(config);
		SqlNode parse = planner.parse("select c.cust_prim_cust_fname, t.cust_id ,t.cust_id,sum(balance) from customer c, transaction_bal t where c.cust_id=t.cust_id group by t.cust_id,c.cust_prim_cust_fname order by t.cust_id");
		 planner.validate(parse);
		 RelRoot relRoot = planner.rel(parse);
		
		 AbstractRelNode myRel =(AbstractRelNode)relRoot.project();
		 /* System.out.println("Tables  "+RelOptUtil.getVariablesUsed(myRel));
		  System.out.println("Fields  "+myRel.getRowType().getFieldNames());
		  System.out.println("Row Type  "+myRel.getRowType());*/
		  
		 
		 Planner planner1 = Frameworks.getPlanner(config);
		absRelNode = (AbstractRelNode) relRoot.project();
		 System.out.println(RelOptUtil.toString(absRelNode));
		 
		 
		SqlNode transaction_bal = planner1.parse("select * from transaction");
		                 
		planner1.validate(transaction_bal);
		
		
		
	    RelRoot relRoot1 = planner1.rel(transaction_bal);
	    RelNode reltransaction_bal =relRoot1.project();
	    printDataInRelNode(reltransaction_bal);
	    System.out.println(reltransaction_bal.getTraitSet().toString());
	    
	    
	    /*AggCall newRel=builder.push(reltransaction_bal);
	    System.out.println(newRel);*/
	    convertSqlToRexNode(parse,config,calCon,absRelNode,reltransaction_bal);
	   
	    System.out.println(reltransaction_bal.getRowType().getFieldList());
	    
	   
      System.out.println("*******************");
      
     
      
      RelDataType relDataType=absRelNode.getRowType();
      RelDataTypeField relField=new RelDataTypeFieldImpl("test", 0, relDataType);
     
   /*    SubstitutionVisitor sb=new SubstitutionVisitor(absRelNode, reltransaction_bal);*/
      
     
      
	}

	

	@SuppressWarnings("deprecation")
	private static void convertSqlToRexNode(SqlNode parse, FrameworkConfig config, Connection connection, RelNode relNode, RelNode rel1) throws SQLException {
		final CalciteServerStatement statement = connection
			      .createStatement().unwrap(CalciteServerStatement.class);
			  final CalcitePrepare.Context prepareContext =
			        statement.createPrepareContext();
			  final JavaTypeFactory typeFactory = prepareContext.getTypeFactory();
			  CalciteCatalogReader catalogReader =
			        new CalciteCatalogReader(prepareContext.getRootSchema(),
			            prepareContext.config().caseSensitive(),
			            prepareContext.getDefaultSchemaPath(),
			            typeFactory);
			  
			  final RexBuilder rexBuilder = new RexBuilder(typeFactory);
			  final RelOptPlanner planner = new VolcanoPlanner(config.getCostFactory(),
			      config.getContext());
			  final RelBuilder builder = RelBuilder.create(config);
			SqlValidatorCatalogReader val= catalogReader;
			
			SqlOperatorTable sbopt=(SqlOperatorTable)val;
			CalciteRelationalAlgbra var1=(CalciteRelationalAlgbra) CalciteRelationalAlgbra.newValidator(sbopt, catalogReader, typeFactory, SqlConformanceEnum.DEFAULT);
			
			System.out.println(parse.getKind());
			  SqlNode nnew=var1.performUnconditionalRewrites(parse, false);
			  
			  if(nnew instanceof SqlSelect){
			    	System.out.println(nnew.getKind());
			/*org.apache.calcite.sql.validate.SqlValidatorImpl.performUnconditionalRewrites 
			  //SqlValidatorImpl var=new SqlValidatorImpl(sb , val, catalogReader.getTypeFactory(),  SqlConformanceEnum.DEFAULT);
			  
			  
			*/
			 
			List<RexNode> rexNode=  (List<RexNode>) rexBuilder.identityProjects(rel1.getExpectedInputRowType(0));
			  
			  List<String> fieldNameList=new ArrayList<String>() ;
			  fieldNameList.add("aaa");
			  fieldNameList.add("ccc");
			  fieldNameList.add("bbb");
			 
	/*	RelNode rel2=RelOptUtil.createProject(rel1,rexNode,fieldNameList);
			  
		 System.out.println(RelOptUtil.toString(rel2));
			  System.out.println("SSSSSSSSSSSSSSSSSSSS");
			  System.out.println(RelOptUtil.toString(rel2));*/
			  
			  SqlCall sqlCall=(SqlCall) parse;
			  List<SqlNode> listSqlNode=sqlCall.getOperandList();
			  
			  System.out.println(listSqlNode.get(3));
			  
			
		/*SqlRexContext cx=SqlToRexConverter.configBuilder()
		
		
		ReflectiveConvertletTable  re=new ReflectiveConvertletTable();
		SqlRexConvertlet sqlRex=re.get((SqlCall) parse);
		RexNode rexNode=sqlRex.convertCall(cx, (SqlCall) parse);*/
			  
			  
			//new Code 
			  RelOptCluster cluster=RelOptCluster.create(planner, rexBuilder);
			 
		      final RelDataType stringType = typeFactory.createJavaType(String.class);
		      final RelDataType integerType = typeFactory.createJavaType(Integer.class);
		      final RelDataType sqlBigInt =
		              typeFactory.createSqlType(SqlTypeName.BIGINT);
		      final Table table = new AbstractTable() {
		          public RelDataType getRowType(RelDataTypeFactory typeFactory) {
		        	  
		            		for(int i=0;i<rexNode.size();i++){
		            			typeFactory.builder()
		            			.add(rexNode.get(i).toString(), rexNode.get(i).getType());
		                       // .add("txn_num", integerType);
		            		}
		            		return typeFactory.builder().build();
		          }
		          
		      };
		     
		      final RelOptAbstractTable t1 = new RelOptAbstractTable(catalogReader,
		              "t1", table.getRowType(typeFactory)) {
		            @Override public <T> T unwrap(Class<T> clazz) {
		              return clazz.isInstance(table)
		                  ? clazz.cast(table)
		                  : super.unwrap(clazz);
		            }
		          };
		          
		           RelNode rt1 = EnumerableTableScan.create(cluster, t1);
		         
		          
		           for(int i=0;i<rexNode.size();i++){
           			typeFactory.builder()
           			.add(rexNode.get(i).toString(), rexNode.get(i).getType());
                      // .add("txn_num", integerType);
           		}
		          
		          RelNode project = LogicalProject.create(rt1,
		                 /* ImmutableList.of(
		                      (RexNode) rexBuilder.makeInputRef(stringType, 0),
		                      rexBuilder.makeInputRef(integerType, 1)),*/
		        		  rexNode,
		                  typeFactory.builder().add("cust_ids", stringType).add("txn_ids", integerType)
		                  .build());
		          System.out.println("project &&&&&&"+ project.getRowType().getFieldList());
		          
		          
		          AggregateCall aggCall = AggregateCall.create(SqlStdOperatorTable.SUM,
		                  false, Collections.singletonList(1), -1, sqlBigInt, "balance");
		              RelNode agg = new LogicalAggregate(cluster,
		                  cluster.traitSetOf(Convention.NONE), project, false,
		                  ImmutableBitSet.of(0), null, Collections.singletonList(aggCall));

		               RelNode rootRel = agg;
		              System.out.println(" RT Test &&&&&&&&   "+RelOptUtil.toString(rootRel));
		              
			  }    //   System.out.println( rootRel.getRowType().getFieldList());
		          
	}

	private static void printDataInRelNode(RelNode reltransaction_bal) throws SQLException {
		// TODO Auto-generated method stub
		/*try(PreparedStatement pd = RelRunners.run(reltransaction_bal)) {
	    ResultSet resultSet = pd.executeQuery();
	    
	   while (resultSet.next()) {
	     System.out.println(resultSet.getString("cust_id")+"--"+resultSet.getString("txn_amt"));
	    }
	}*/
	}
	 public static SqlValidatorWithHints newValidator(
		      SqlOperatorTable opTab,
		      SqlValidatorCatalogReader catalogReader,
		      RelDataTypeFactory typeFactory,
		      SqlConformance conformance) {
		    return new CalciteRelationalAlgbra(opTab, catalogReader, typeFactory,
		        conformance);
		  }

}
