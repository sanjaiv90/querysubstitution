package changeQuery;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

public class SetConfigData {
	Connection con;

	   public void createMetadataDbConnection(){
				try {
					Class.forName(Configuration.getMetaDataDriver());
					System.out.println(Configuration.getMetaDataDriver());
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					con = DriverManager.getConnection(Configuration.getMetaDataUrl());
					//System.out.println(Configuration.getMetaDataUrl());
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
		}
	   
	public void setConfigData() throws SQLException{
		setCalculationConfig();
		setPadListConfig();
		setPadDataDbConfig();
		setListTables();
	}
	
	

	public void setCalculationConfig() throws SQLException{
		System.out.println("Connections   :: "+con);
	 Statement stmt = con.createStatement();
		String query="select * from Calculation c ,column_dtl cd ,lf_col_xref x,logical_file l where (c.date_field=cd.col_id) and c.date_field=x.col_id and  x.lf_id=l.lf_id and c.calc_id='pad_ops_001'";
	    ResultSet rs = stmt.executeQuery(query);
		
			while(rs.next()){
				Configuration.setAcctDateColumn(rs.getString("col_short_name"));  
				Configuration.setCalculatedCol(rs.getString("calculation"));
				Configuration.setCalculationTable(rs.getString("calculation_table"));
				Configuration.setCalculationOnTable(rs.getString("lf_name"));
				
			}
	}
	public void setPadListConfig() throws SQLException{
		setHashLookupByTableColumn();
		setHashLookupByDateAndPad();
		setHashLookupByPadsInfo();
	       }
	 void setHashLookupByTableColumn() throws SQLException{
		 Map<String,Integer> hashTableCol=new HashMap<String,Integer>();
		 
			Statement stmt = con.createStatement();
			String query="select * from lf_col_xref l , column_dtl cd,logical_file lf where l.col_id=cd.col_id and l.lf_id=lf.lf_id";
			ResultSet rs = stmt.executeQuery(query);
		    while (rs.next()){
		        	hashTableCol.put(rs.getString("lf_name").toLowerCase()+"."+rs.getString("col_short_name").toLowerCase(), rs.getInt("lf_col_xref_id"));
		    }
		    Configuration.setHashTableColAndId(hashTableCol);
		    
	}
	 void setHashLookupByPadsInfo() throws SQLException{
		 Map<String,Integer> hashPadAndCount=new HashMap<String,Integer>();
		 
		 Multimap<String,Integer> hashPadAndId = ArrayListMultimap.create();
			Statement stmt = con.createStatement();
			String query="select * from lf_col_xref x, operation o where x.lf_id=o.to_lf and o.is_avilable";
			ResultSet rs = stmt.executeQuery(query);
		    while (rs.next()){
		    	hashPadAndCount.put(rs.getString("lf_id"), rs.getInt("number_of_keys"));
		    	hashPadAndId.put(rs.getString("lf_id"), rs.getInt("col_id"));
		    }
		    Configuration.setHashPadAndCount(hashPadAndCount);
		    Configuration.setHashPadAndId(hashPadAndId);
	}		
	 void setHashLookupByDateAndPad() throws SQLException{
		
		 
		 Multimap<String,String> hashDateAndPad = ArrayListMultimap.create();
			Statement stmt = con.createStatement();
			String query=" select * from calculation c,operation o,column_dtl cd where c.calc_id=o.ops and c.date_field=cd.col_id "
					+ "and c.calc_id in(select o.ops from operation o where o.ops like 'pad_ops%')";
			ResultSet rs = stmt.executeQuery(query);
		    while (rs.next()){
		    	hashDateAndPad.put(rs.getString("col_short_name"), rs.getString("to_lf"));
		    	
		    }
		    Configuration.setHashDateAndPad(hashDateAndPad);
	}		
	  void setPadDataDbConfig() throws SQLException {
		  Statement stmt = con.createStatement();
			String query="select * from pf_ds_xref p ,datasource d ,dburl u"
                         +" where p.ds_id=d.ds_id and d.db_url_id=u.db_url_id and p.pf_id=(select to_pf from operation limit 1)";
			ResultSet rs = stmt.executeQuery(query);
		    while (rs.next()){
		    	String schema=rs.getString("schema");
		    	
		    	String padDataUrlWithDb=rs.getString("db_url")+"://"+rs.getString("server")+":"+rs.getString("port")+"/" +schema;
				Configuration.setPadDataUrlWithDb(padDataUrlWithDb);
				Configuration.setPadDataUser(rs.getString("user"));	
				Configuration.setPadDataPassword(rs.getString("password"));
				Configuration.setPadDataDriver(rs.getString("driver"));
				Configuration.setPadDataCatalog(schema);
		    }
		}
	  private void setListTables() throws SQLException {
		  List<String> arrTables=new ArrayList<String>();
		  Statement stmt = con.createStatement();
			String query="select lf_name from logical_file where lf_id like 'LF%'";
			ResultSet rs = stmt.executeQuery(query);
		    while (rs.next()){
		    	arrTables.add(rs.getString(1));
		    	
		    }
		
			Configuration.setTables(arrTables);
		}

}
