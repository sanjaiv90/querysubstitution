package changeQuery;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.Scanner;

public class ConfigFileReader {
	 @SuppressWarnings("unused")
	public void setDbUrlAndDriverFormConfigFile() throws IOException{
    	 String url="";
    	
    	 Properties prop=new Properties();
    	 String propFile="config.properties";
    	 
    	
    	// ClassLoader classLoader = getClass().getClassLoader();
    	 InputStream inSteam = getClass().getResourceAsStream("../file/config.properties");
    	 /* File file = new File(classLoader.getResource("/file/config.properties").getFile());
    	 
    	// InputStream inSteam=new FileInputStream(file);
    	// System.out.println(file);
    	/* InputStream inSteam=new FileInputStream(propFile);*/
    			
    	 if(inSteam!=null)
    	 prop.load(inSteam);
    	 else {
				throw new FileNotFoundException("property file '" + inSteam + "' not found in the classpath");
			}
    	 url=prop.getProperty("url") +prop.getProperty("db")+"?user="+prop.getProperty("user")+"&password="+prop.getProperty("password");
    	 String driver=prop.getProperty("driver");
    	 Configuration.setMetaDataUrl(url);
    	 Configuration.setMetaDataDriver(driver);
    	 System.out.println("Meta Url:"+url);
    	 System.out.println("Driver: "+driver);
     }
	
}
