package changeQuery;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

import org.apache.calcite.sql.SqlNodeList;

public class CreateUnionPadQuery {

	
	public String getPadQuery(Set<String> fieldsSelect, SqlNodeList groupBy, String padKey, Date txnDate,
			String calCulatedColumn, String tableAlias, Set<String> fieldsInWhere, String whereCondition) {
		Set<String> allField=fieldsSelect;
		if(fieldsInWhere!=null)
		{
		allField.addAll(fieldsInWhere);
		}
		ParseUtil parseUtil=new ParseUtil();
		String getAllSelectedColumn=parseUtil.createSetToCommaString(allField);
		
		String newGroup="";
		/*if(groupBy!=null)*/
		newGroup=parseUtil.createSetToCommaString(fieldsSelect);
		 Calendar txnCalenderDate = Calendar.getInstance();
		 txnCalenderDate.setTime(txnDate);
		 DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		 String accDate = df.format(txnDate);
		String monthlyQuery=calculateMonthly(getAllSelectedColumn,txnCalenderDate,padKey,newGroup,whereCondition);
		String dailyQuery=calculateDaily(getAllSelectedColumn,txnCalenderDate,padKey,newGroup,whereCondition);
		String combineQuery=combineMonthlyDaily(monthlyQuery,dailyQuery,calCulatedColumn,getAllSelectedColumn,accDate,tableAlias,newGroup);
		return combineQuery;
	}

	
	private String calculateMonthly(String selectAll, Calendar txn_Date, String padKey, String newGroup, String whereCondition) {
		StringBuffer sb=new StringBuffer();
		sb.append("select "+selectAll+" , sum("+ getSumOfMonth(txn_Date.get(Calendar.MONTH))+") as balance from "+padKey+"  k, "+padKey+"_monthly"
				+ " m where k.pad_key_id=m.pad_key_id and m.year="+txn_Date.get(Calendar.YEAR));
		if(whereCondition !="")
			sb.append(" and "+whereCondition);
		/*if(newGroup=="") 
		sb.append(" group by "+Configuration.getGroupByColumn());
		else*/
			sb.append(" group by "+newGroup);
		return sb.toString();
	}
	
	private String calculateDaily(String select, Calendar txn_Date, String padKey, String newGroup, String whereCondition) {
			
		StringBuffer sb=new StringBuffer();
		sb.append("select "+select+" , sum("+ getSumOfDate(txn_Date.get(Calendar.DAY_OF_MONTH))+") as balance from "+padKey+"  k, "+padKey+"_daily"
				+ " d where k.pad_key_id=d.pad_key_id and d.year="+txn_Date.get(Calendar.YEAR)+" and d.month="+(txn_Date.get(Calendar.MONTH)+1));
		
		
		if(whereCondition !="")
			sb.append(" and "+whereCondition);
		/*if(newGroup=="") 
			sb.append(" group by "+Configuration.getGroupByColumn());
			else*/
				sb.append(" group by "+newGroup);
			
		return sb.toString();
	}
	
	private String getSumOfMonth(int month) {
		month=month+1;
			StringBuffer sb=new StringBuffer();
			sb.append("begin_bal");
			for(int i=1;i<month;i++)
				sb.append("+m"+i);
			return sb.toString();
	}

	private String getSumOfDate(int DAY_OF_MONTH) {
		StringBuffer sb=new StringBuffer();
		for(int i=1;i<=DAY_OF_MONTH;i++)
			sb.append("d"+i+"+");
		String sumOfDate=sb.toString();
		return sumOfDate.substring(0,sumOfDate.length()-1);
	}
	private String combineMonthlyDaily(String monthlyQuery, String dailyQuery, String calCulatedColumn, String select, String accDate, String tableAlias, String newGroup) {
		StringBuffer sb=new StringBuffer();
		
		sb.append("Select * from (select k.*, '"+accDate+"' as acct_dt from ("+monthlyQuery+" union all "+dailyQuery+")k ");
		
			sb.append(" group by "+ newGroup);
		sb.append("  ) as k");
		/*if(Configuration.getCalculationTable().equals(tableAlias))
			sb.append(tableAlias);*/
		return sb.toString();
	}
}
