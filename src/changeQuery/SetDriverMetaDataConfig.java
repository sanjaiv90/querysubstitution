package changeQuery;

import java.io.IOException;
import java.sql.SQLException;

import org.mariadb.jdbc.MariaDbConnection;

public class SetDriverMetaDataConfig {

	public static void setMetaData(){
		ConfigFileReader classObj=new ConfigFileReader();
	    try {
	    	classObj.setDbUrlAndDriverFormConfigFile();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
 		SetConfigData config=new SetConfigData();
 		config.createMetadataDbConnection();
 		try {
			config.setConfigData();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		Configuration.setMetaLoaded(true);
	}
}
