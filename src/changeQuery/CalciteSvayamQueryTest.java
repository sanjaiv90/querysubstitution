package changeQuery;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.apache.calcite.adapter.jdbc.JdbcSchema;
import org.apache.calcite.jdbc.CalciteConnection;
import org.apache.calcite.schema.SchemaPlus;

public class CalciteSvayamQueryTest {
	
	 public static void main(String[] args) throws SecurityException, IOException, ClassNotFoundException, SQLException {
	   
	    
	    
		 System.out.println( "Hello World!" );
		 String url="jdbc:mysql://localhost:3306/SVAYAM_DEV";
		 String driverClassName="org.mariadb.jdbc.Driver";
		 String username="root";
		 String password="fun2learn";
		 
		 Connection calCon=DriverManager.getConnection("jdbc:calcite:");
		 CalciteConnection calciteConnection=calCon.unwrap(CalciteConnection.class);
		 SchemaPlus schemaPlus=calciteConnection.getRootSchema();
		
		final DataSource ds=JdbcSchema.dataSource(url, driverClassName, username, password);
		schemaPlus.add("svayam_dev", JdbcSchema.create(schemaPlus, "svayam_dev", ds,null,null));
		//Database Query
		Statement stmt = calciteConnection.createStatement();
		String query ="select \"cust_prim_cust_fname\",\"cust_prim_cust_addr_city\" from"
        		+ " \"svayam_dev\".\"customer\" c, \"svayam_dev\".\"transaction\" t "
        		+ " where  c.\"cust_id\"=t.\"cust_id\" ";
		/*String query1 ="select * from"
        		+ " \"svayam_dev\".\"customer\" c, \"svayam_dev\".\"transaction\" t "
        		+ " where  c.\"cust_id\"=t.\"cust_id\" ";*/
			 ResultSet rs = stmt.executeQuery(query);
		//Database Meta Data 
	    /* DatabaseMetaData dbm=  calCon.getMetaData();
	     ResultSet rs= dbm.getTables("svayam_dev", null, null,null); */
        while (rs.next()) {
            System.out.println(rs.getString(1) + '=' + rs.getString(2));
        }
	 }
}
