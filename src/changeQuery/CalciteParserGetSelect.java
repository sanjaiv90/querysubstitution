package changeQuery;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.calcite.adapter.java.JavaTypeFactory;
import org.apache.calcite.adapter.jdbc.JdbcSchema;
import org.apache.calcite.config.Lex;
import org.apache.calcite.jdbc.CalciteConnection;
import org.apache.calcite.jdbc.CalcitePrepare;
import org.apache.calcite.plan.RelTraitDef;
import org.apache.calcite.prepare.CalciteCatalogReader;
import org.apache.calcite.rel.type.RelDataTypeFactory;
import org.apache.calcite.schema.SchemaPlus;
import org.apache.calcite.server.CalciteServerStatement;
import org.apache.calcite.sql.SqlNode;
import org.apache.calcite.sql.SqlOperatorTable;
import org.apache.calcite.sql.SqlOrderBy;
import org.apache.calcite.sql.SqlSelect;
import org.apache.calcite.sql.parser.SqlParseException;
import org.apache.calcite.sql.parser.SqlParser;
import org.apache.calcite.sql.validate.SqlConformance;
import org.apache.calcite.sql.validate.SqlConformanceEnum;
import org.apache.calcite.sql.validate.SqlValidatorCatalogReader;
import org.apache.calcite.sql.validate.SqlValidatorImpl;
import org.apache.calcite.sql.validate.SqlValidatorWithHints;
import org.apache.calcite.tools.FrameworkConfig;
import org.apache.calcite.tools.Frameworks;
import org.apache.calcite.tools.Planner;
import org.apache.calcite.tools.Programs;
import org.apache.calcite.tools.RelConversionException;
import org.apache.calcite.tools.ValidationException;
import org.mariadb.jdbc.Driver;
import org.mariadb.jdbc.internal.logging.Logger;
import org.mariadb.jdbc.internal.logging.LoggerFactory;
import org.mariadb.jdbc.internal.util.dao.QueryException;

public class CalciteParserGetSelect extends SqlValidatorImpl {
	 private static Logger logger = LoggerFactory.getLogger(CalciteParserGetSelect.class);

	
	public CalciteParserGetSelect(SqlOperatorTable opTab, SqlValidatorCatalogReader catalogReader,
			RelDataTypeFactory typeFactory, SqlConformance conformance ) {
		   super(opTab, catalogReader, typeFactory, conformance);
		    // TODO Auto-generated constructor stub
	}
	
	@SuppressWarnings("deprecation")
	public static SqlNode parseQuery(String query) throws SQLException, SqlParseException, RelConversionException {
		
		logger.info("Parse query"+query);
		
		/* String url=Configuration.getPadDataUrlWithDb();
		 String driverClassName=Configuration.getPadDataDriver();
		 String username=Configuration.getPadDataUser();
		 String password=Configuration.getPadDataPassword();*/
		  
		 try {
			Class.forName("org.apache.calcite.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 Connection calCon=DriverManager.getConnection("jdbc:calcite:");
		 CalciteConnection calciteConnection=calCon.unwrap(CalciteConnection.class);
		 SchemaPlus rootSchema=calciteConnection.getRootSchema();
		 //define JDBC configuration data source
		 final DataSource dataSource=JdbcSchema.dataSource(Configuration.getPadDataUrlWithDb(), Configuration.getPadDataDriver(), 
				                                            Configuration.getPadDataUser(), Configuration.getPadDataPassword());
		final FrameworkConfig config =
		        Frameworks.newConfigBuilder()
		                .parserConfig(SqlParser.configBuilder().setLex(Lex.MYSQL).build())
		                .defaultSchema(rootSchema.add("",
		                        JdbcSchema.create(rootSchema, "Dev",
		dataSource, null, null)))
		                .traitDefs((List<RelTraitDef>) null)

		.programs(Programs.heuristicJoinOrder(Programs.RULE_SET, true, 2))
		                .build();
 		Planner planner = Frameworks.getPlanner(config);
 		
		SqlNode parseNode = planner.parse(query);
		
		try {
			planner.validate(parseNode);
		} catch (ValidationException e) {
			// TODO Auto-generated catch block
				try {
					throw new ValidationException(e.toString());
				} catch (ValidationException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		}
		if( parseNode  instanceof SqlOrderBy){
			parseNode= convertOrderByToSelectNode(config,calCon,parseNode);
              
	  }   
	  return parseNode;
      
	}

	

	private static SqlNode convertOrderByToSelectNode(FrameworkConfig config, Connection connection, SqlNode orderBy) throws SQLException {
		final CalciteServerStatement statement = connection
			      .createStatement().unwrap(CalciteServerStatement.class);
			  final CalcitePrepare.Context prepareContext =
			        statement.createPrepareContext();
			  final JavaTypeFactory typeFactory = prepareContext.getTypeFactory();
			  CalciteCatalogReader catalogReader =
			        new CalciteCatalogReader(prepareContext.getRootSchema(),
			            prepareContext.config().caseSensitive(),
			            prepareContext.getDefaultSchemaPath(),
			            typeFactory);
			
			SqlOperatorTable operatorTable=(SqlOperatorTable)catalogReader;
			CalciteParserGetSelect OrderByTOSelectValidotor=(CalciteParserGetSelect) CalciteParserGetSelect.newValidator(operatorTable, catalogReader, typeFactory, SqlConformanceEnum.DEFAULT);
			
			
			  SqlNode select=OrderByTOSelectValidotor.performUnconditionalRewrites(orderBy, false);
			  
			  if( select  instanceof SqlSelect){
			    	System.out.println(select.getKind());
		              
			  }   
		    return select;      
	}

	 public static SqlValidatorWithHints newValidator(
		      SqlOperatorTable opTab,
		      SqlValidatorCatalogReader catalogReader,
		      RelDataTypeFactory typeFactory,
		      SqlConformance conformance) {
		    return new CalciteRelationalAlgbra(opTab, catalogReader, typeFactory,
		        conformance);
		  }


}
