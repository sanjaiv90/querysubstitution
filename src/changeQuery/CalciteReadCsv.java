package changeQuery;

import java.io.File;
import java.io.FilenameFilter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.Properties;

import org.apache.calcite.jdbc.CalciteConnection;



public class CalciteReadCsv {

	
	public static void main(String[] args) throws Exception {
	    new CalciteReadCsv().run();
	  }

	  public void run() throws ClassNotFoundException, SQLException {
	    Class.forName("org.apache.calcite.jdbc.Driver");
	    Properties info = new Properties();
	    info.setProperty("lex", "JAVA");
	    Connection connection =
	        DriverManager.getConnection("jdbc:calcite:model="
	                + "c://app/configJson",info);
	    CalciteConnection calciteConnection =
	        connection.unwrap(CalciteConnection.class);
	    //SchemaPlus rootSchema = calciteConnection.getRootSchema();
	    //rootSchema.add("os", new ReflectiveSchema(new Os()));
	    Statement statement = connection.createStatement();
	    ResultSet resultSet =
	            statement.executeQuery("select stream * from SS.ORDERS where SS.ORDERS.UNITS > 5");
	    final StringBuilder buf = new StringBuilder();
	    while (resultSet.next()) {
	      int n = resultSet.getMetaData().getColumnCount();
	      for (int i = 1; i <= n; i++) {
	        buf.append(i > 1 ? "; " : "")
	            .append(resultSet.getMetaData().getColumnLabel(i))
	            .append("=")
	            .append(resultSet.getObject(i));
	      }
	      System.out.println(buf.toString());
	      buf.setLength(0);
	    }
	    resultSet.close();
	    statement.close();
	    connection.close();
	  }

}
