package changeQuery;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Multimap;

public class Configuration {

	private static boolean isMetaLoaded;

//	private static String svayamCatalog;
	private static String acctDateColumn;
	private static List<String> padIds;
	private static String calculatedCol;
	private static String calculationTable;
	private static String calculationOnTable;

	private static List<String> tables;
	private static String newAddedTable;
	private static String defaultGroup;
	private static Map<String, Integer> hashTableColAndId;
	private static Map<String, Integer> hashPadAndCount;
	private static Multimap<String, Integer> hashPadAndId;
	private static Multimap<String, String> hashDateAndPad;
	private static String metaDataUrl;
	private static String metaDataDriver;

	private static String padDataUrlWithDb;
	private static String padDataDriver;
	private static String padDataUser;
	private static String padDataPassword;
	private static String padDataCatalog;

<<<<<<< HEAD
	public static String getSvayamCatalog(){
		return "svayam_data";
=======
	/*public static String getSvayamCatalog() {
		return "svayam_dev";
>>>>>>> 96e1190e0d92e774a48885b593e03964bcfb8a26
	}

	public static void setSvayamCatalog(String svayamCatalog) {
		Configuration.svayamCatalog = svayamCatalog;
	}*/

	public static String getAcctDateColumn() {
		return acctDateColumn;
	}

	public static void setAcctDateColumn(String acctDateColumn) {
		Configuration.acctDateColumn = acctDateColumn;
	}

	public static List<String> getPadIds() {
		return padIds;
	}

	public static void setPadIds(List<String> padIds) {
		Configuration.padIds = padIds;
	}

	public static String getCalculatedCol() {
		return calculatedCol;
	}

	public static void setCalculatedCol(String calculatedCol) {
		Configuration.calculatedCol = calculatedCol;
	}

	public static String getCalculationTable() {
		return "transaction_bal";
	}

	public static void setCalculationTable(String calculationTable) {
		Configuration.calculationTable = calculationTable;
	}

	public static String getCalculationOnTable() {
		return calculationOnTable;
	}

	public static void setCalculationOnTable(String calculationOnTable) {
		Configuration.calculationOnTable = calculationOnTable;
	}

	public static String getNewAddedTable() {
		return newAddedTable;
	}

	public static void setNewAddedTable(String newAddedTable) {
		Configuration.newAddedTable = newAddedTable;
	}

	public static String getDefaultGroup() {
		return defaultGroup;
	}

	public static void setDefaultGroup(String defaultGroup) {
		Configuration.defaultGroup = defaultGroup;
	}

	public static Map<String, Integer> getHashTableColAndId() {
		return hashTableColAndId;
	}

	public static void setHashTableColAndId(Map<String, Integer> hashTableColAndId) {
		Configuration.hashTableColAndId = hashTableColAndId;
	}

	public static Map<String, Integer> getHashPadAndCount() {
		return hashPadAndCount;
	}

	public static void setHashPadAndCount(Map<String, Integer> hashPadAndCount) {
		Configuration.hashPadAndCount = hashPadAndCount;
	}

	public static Multimap<String, Integer> getHashPadAndId() {
		return hashPadAndId;
	}

	public static void setHashPadAndId(Multimap<String, Integer> hashPadAndId) {
		Configuration.hashPadAndId = hashPadAndId;
	}

	public static Multimap<String, String> getHashDateAndPad() {
		return hashDateAndPad;
	}

	public static void setHashDateAndPad(Multimap<String, String> hashDateAndPad) {
		Configuration.hashDateAndPad = hashDateAndPad;
	}

	public static boolean isMetaLoaded() {
		return isMetaLoaded;
	}

	public static void setMetaLoaded(boolean isMetaLoaded) {
		Configuration.isMetaLoaded = isMetaLoaded;
	}

	public static String getMetaDataUrl() {
		return metaDataUrl;
	}

	public static void setMetaDataUrl(String metaDataUrl) {
		Configuration.metaDataUrl = metaDataUrl;
	}

	public static String getMetaDataDriver() {
		return metaDataDriver;
	}

	public static void setMetaDataDriver(String metaDataDriver) {
		Configuration.metaDataDriver = metaDataDriver;
	}

	public static String getPadDataUrlWithDb() {
		return padDataUrlWithDb;
	}

	public static void setPadDataUrlWithDb(String padDataUrlWithDb) {
		Configuration.padDataUrlWithDb = padDataUrlWithDb;
	}

	public static String getPadDataDriver() {
		return padDataDriver;
	}

	public static void setPadDataDriver(String padDataDriver) {
		Configuration.padDataDriver = padDataDriver;
	}

	public static String getPadDataUser() {
		return padDataUser;
	}

	public static void setPadDataUser(String padDataUser) {
		Configuration.padDataUser = padDataUser;
	}

	public static String getPadDataPassword() {
		return padDataPassword;
	}

	public static void setPadDataPassword(String padDataPassword) {
		Configuration.padDataPassword = padDataPassword;
	}

	public static String getPadDataCatalog() {
		return padDataCatalog;
	}

	public static void setPadDataCatalog(String padDataCatalog) {
		Configuration.padDataCatalog = padDataCatalog;
	}

	public static List<String> getTables() {
		return tables;
	}

	public static void setTables(List<String> tables) {
		Configuration.tables = tables;
	}

}
