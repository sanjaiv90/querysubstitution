 package changeQuery;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.calcite.sql.SqlDialect;
import org.apache.calcite.sql.SqlNode;
import org.apache.calcite.sql.SqlNodeList;
import org.apache.calcite.sql.SqlSelect;
import org.apache.calcite.sql.parser.SqlParseException;
import org.apache.calcite.tools.RelConversionException;
import org.apache.calcite.tools.ValidationException;
import org.mariadb.jdbc.MariaDbConnection;
import org.mariadb.jdbc.internal.logging.Logger;
import org.mariadb.jdbc.internal.logging.LoggerFactory;
import org.mariadb.jdbc.internal.util.dao.QueryException;

import beans.TableAlias;

public class SvayamParserSqlQuery {
	
	 private static Logger logger = LoggerFactory.getLogger(SvayamParserSqlQuery.class);
	public static Boolean IsChangeQuery(String str, Connection connection){
		   
	
		  boolean isCalculatedColummn=false;
          if(Configuration.getCalculationTable()!=null&& str.indexOf(Configuration.getCalculationTable())>-1/* && str.indexOf(Configuration.getCalculatedCol())>-1*/){
          	isCalculatedColummn=true;
          }
		    return isCalculatedColummn;
		}
	
	 public static String changeQuery(String str) throws QueryException, SQLException, SqlParseException, ValidationException, RelConversionException{
		 if(!Configuration.isMetaLoaded())
		SetDriverMetaDataConfig.setMetaData();
		 SvayamParserSqlQuery objSvayamParserSqlQuery=new SvayamParserSqlQuery();
		 String newQuery=null;
		 SqlNodeList select=null;
		 SqlNode where=null;
		 SqlNode from=null;
		 SqlSelect sqlSelect=null;
		 Set<String> fieldsSelect=null;
    	 String newWhere=null;
    	 SqlNodeList groupBy=null;
    	 String newGroup=null;
    	 Date txnDate=null;
    	 String tableAlias=null;
    	 Set<String> fieldsInWhere=null;
    	 List<TableAlias> listCombindTableAlias=null;
    	 Map<String,String> allTableAndAlias=null;
		 SqlNode parsedStatement = CalciteParserGetSelect.parseQuery(str);
		 String whereCondition="";
		 if(parsedStatement instanceof SqlSelect){
		    	//System.out.println(parsedStatement.getKind());
		    	sqlSelect=(SqlSelect)parsedStatement;
		    	 select=sqlSelect.getSelectList();     
		    	 where=sqlSelect.getWhere();
		    	 from=sqlSelect.getFrom();
		    	 tableAlias=objSvayamParserSqlQuery.getCalCulatedTableAlias(from.toSqlString(SqlDialect.DUMMY).getSql());
		    	 listCombindTableAlias=objSvayamParserSqlQuery.getCombindTableAlias(from.toSqlString(SqlDialect.DUMMY).getSql());
		    	 allTableAndAlias=objSvayamParserSqlQuery.getAllTableAlias(from.toSqlString(SqlDialect.DUMMY).getSql());
		    	 groupBy=sqlSelect.getGroup();
		    	
		    	if(select!=null)
		    		fieldsSelect=objSvayamParserSqlQuery.changeSelectWithPad(select.toSqlString(SqlDialect.DUMMY).getSql(),allTableAndAlias);
		    	if(where==null){
		    		  throw new QueryException("Column "+Configuration.getAcctDateColumn()+" not found");
		    	}
		    	else{
		    	 txnDate=objSvayamParserSqlQuery.getTxnDate(where.toSqlString(SqlDialect.DUMMY).getSql());
		    	 fieldsInWhere=objSvayamParserSqlQuery.getFieldInWhere(where.toSqlString(SqlDialect.DUMMY).getSql(),allTableAndAlias);
		    	 whereCondition=objSvayamParserSqlQuery.getWhereCondition(where.toSqlString(SqlDialect.DUMMY).getSql(),listCombindTableAlias,tableAlias);
		    	}
  		    	
		    	 if(txnDate==null)
		    		  throw new QueryException("Column "+Configuration.getAcctDateColumn()+" not found");
		    
		    	 /*if(groupBy!=null)
		    		 newGroup=groupBy.toSqlString(SqlDialect.DUMMY).getSql();*/
		    }
		    PadSelection padSelection=new PadSelection();
		    String padId=padSelection.selectBestPad(fieldsSelect,fieldsInWhere,Configuration.getAcctDateColumn());
		    if(padId=="")
		    	 throw new QueryException("Suitable pad not fount ");
		    
		    CreateUnionPadQuery objCreatePadQuery=new CreateUnionPadQuery();
		    String padQUery= objCreatePadQuery.getPadQuery(fieldsSelect, groupBy, padId, txnDate, Configuration.getCalculatedCol(),tableAlias,fieldsInWhere,whereCondition);
		  //  str= str.replaceFirst(Configuration.getCalculationTable(), padQUery);
		    
		  //  String finalPad="Select * from "+padQUery+"";
		   /* if(!Configuration.getCalculationTable().equals(tableAlias))
		    	finalPad+= " k"	;*/
		    return padQUery;
		 
	 }
	
	

	private  Map<String, String> getAllTableAlias(String sql) {
		// TODO Auto-generated method stub
		Map<String, String> allTableAlias=new HashMap<String, String>();
		sql= sql.toLowerCase();
		  if(sql.indexOf("as")>-1){
		  String[] strArr=sql.replace("`", "").replace("*.", "").replace("\r\n"," ").replace(",","") .split(" ");
		  for(int i=0;i<strArr.length-2;i++){
		  if(strArr[i+1].equals("as")){
			  allTableAlias.put(strArr[i+2],strArr[i]);
		     }
		   }
		  }
		
		return allTableAlias;
		
	}

	private  String getCalCulatedTableAlias(String from) {
		 
		String alias=null;
		from= from.toLowerCase();
		  if(from.indexOf("as")>-1){
		  String[] strArr=from.replace("`", "").replace("*.", "").replace("\r\n"," ").replace(",","") .split(" ");
		  for(int i=0;i<strArr.length-2;i++){
		  if(Configuration.getCalculationTable().equals(strArr[i]) &&  strArr[i+1].equals("as")){
			  alias=strArr[i+2];
			break;
		     }
 		   }
		  }
		return alias;
	}
	  private  List<TableAlias> getCombindTableAlias(String from) {
		  List<TableAlias> listAlias=new ArrayList<TableAlias>();
		String alias=null;
		from= from.toLowerCase();
		  if(from.indexOf("as")>-1){
		  String[] strArr=from.replace("`", "").replace("*.", "").replace("\r\n"," ").replace(",","") .split(" ");
		  for(int i=0;i<strArr.length-2;i++){
			  
		  if(Configuration.getTables().contains(strArr[i]) &&   strArr[i+1].equals("as")){
			  TableAlias tableAlias=new TableAlias();
			  tableAlias.setTableName(strArr[i]);
			  tableAlias.setAlias(strArr[i+2]);
			  listAlias.add(tableAlias);
		      }
		    }
		  }
		return listAlias; 
	}
	
	  private  Set<String> changeSelectWithPad(String select, Map<String, String> allTableAndAlias) {
			select=select.toLowerCase();
		//	select=select.replaceAll("\\s+","");
			String[] arrSelect=select.replace("`", "").split(",");
			ParseUtil ObjUtil=new ParseUtil();
			Set<String> listSelect=ObjUtil.getSelectList(arrSelect,Configuration.getCalculatedCol(),Configuration.getAcctDateColumn(),allTableAndAlias);
			return listSelect;
		}
	
	
	
    private Date getTxnDate(String where) {
    	System.out.println(where);
    	String strDate=null;
    	java.util.Date txn_Date=null;
    	if(where.lastIndexOf(Configuration.getAcctDateColumn())>0){
    	where=where.replaceAll("\\s+","").replace("`", "").replace("'","");
    	int startIndex=where.lastIndexOf(Configuration.getAcctDateColumn())+Configuration.getAcctDateColumn().length()+1;
    	
    		strDate=where.substring(startIndex, startIndex+10);
    	//System.out.println("String Date ******************"+strDate);
    		DateFormat  formatter = new SimpleDateFormat("yyyy-MM-dd");
    	  try {
			txn_Date=formatter.parse(strDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	}
		return txn_Date;
	}
    private  String getWhereCondition(String where, List<TableAlias> listCombindTableAlias, String tableAlias) {
		// TODO Auto-generated method stub
    	where=where.toLowerCase();
   	
   	String[] splitWhere=where.replace("`", "").split("and");
   	ParseUtil ObjUtil=new ParseUtil();
   	String whereCon=ObjUtil.getWhereCondition(splitWhere,Configuration.getCalculatedCol(),Configuration.getAcctDateColumn(),listCombindTableAlias,tableAlias);
   	return whereCon;
		
	}
    
    private  Set<String> getFieldInWhere(String where, Map<String, String> allTableAndAlias) {
	where=where.toLowerCase();
	String[] splitWhere=where.replace("`", "").split("and");
	ParseUtil ObjUtil=new ParseUtil();
	Set<String> listSelect=ObjUtil.getWhereList(splitWhere,Configuration.getCalculatedCol(),Configuration.getAcctDateColumn(),allTableAndAlias);
	return listSelect;
}
  
}
