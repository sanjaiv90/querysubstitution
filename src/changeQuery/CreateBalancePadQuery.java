package changeQuery;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CreateBalancePadQuery {

	public static String getPadQuery(String select, String whereCondtion, String padKey, Date txnDate,
			String calCulatedColumn) {
		 Calendar txnCalenderDate = Calendar.getInstance();
		 txnCalenderDate.setTime(txnDate);
		 DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		 String accDate = df.format(txnDate);
		String monthlyQuery=calculateMonthly(select,whereCondtion,txnCalenderDate,padKey);
		String dailyQuery=calculateDaily(select,whereCondtion,txnCalenderDate,padKey);
		String combineQuery=combineMonthlyDaily(monthlyQuery,dailyQuery,calCulatedColumn,select,accDate);
		return combineQuery;
	}

	private static String calculateMonthly(String select, String whereCondtion, Calendar txn_Date, String padKey) {
		StringBuffer sb=new StringBuffer();
		sb.append("select "+select+" ,sum("+ getSumOfMonth(txn_Date.get(Calendar.MONTH))+") as bal from id"+padKey+"_pad_key1 k, id"+padKey+"_pad_monthly1"
				+ " m where k.pad_key_id=m.pad_key_id and m.year="+txn_Date.get(Calendar.YEAR));
		if(whereCondtion!=null &&whereCondtion !="")
			sb.append(" and "+whereCondtion);
		sb.append(" group by "+select);
		return sb.toString();
	}
	
	private static String calculateDaily(String select, String whereCondtion, Calendar txn_Date, String padKey) {
			
		StringBuffer sb=new StringBuffer();
		sb.append("select "+select+" ,sum("+ getSumOfDate(txn_Date.get(Calendar.DAY_OF_MONTH))+") as bal from id"+padKey+"_pad_key1 k, id"+padKey+"_pad_daily1"
				+ " d where k.pad_key_id=d.pad_key_id and d.year="+txn_Date.get(Calendar.YEAR)+" and d.month="+(txn_Date.get(Calendar.MONTH)+1));
		
		
		if(whereCondtion!=null &&whereCondtion !="")
			sb.append(" and "+whereCondtion);
		sb.append(" group by "+select);
		return sb.toString();
	}
	
	private static String getSumOfMonth(int month) {
		month=month+1;
			StringBuffer sb=new StringBuffer();
			sb.append("begin_bal");
			for(int i=1;i<month;i++)
				sb.append("+m"+i);
			return sb.toString();
	}

	private static String getSumOfDate(int DAY_OF_MONTH) {
		StringBuffer sb=new StringBuffer();
		for(int i=1;i<=DAY_OF_MONTH;i++)
			sb.append("d"+i+"+");
		String sumOfDate=sb.toString();
		return sumOfDate.substring(0,sumOfDate.length()-1);
	}
	private static String combineMonthlyDaily(String monthlyQuery, String dailyQuery, String calCulatedColumn, String select, String accDate) {
		StringBuffer sb=new StringBuffer();
		sb.append("select "+select+", '"+accDate+"' as acct_dt, sum(bal) as "+calCulatedColumn+" from ("+monthlyQuery+" union all "+dailyQuery+")k group by "+select+"");
		return sb.toString();
	}
}
