package changeQuery;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.google.common.collect.Multimap;

public class PadSelection {

	String selectBestPad(Set<String> fieldsSelect, Set<String> fieldsInWhere, String dateCol){
 		String bestPad="";
		List<String> fieldsInQuery= new ArrayList<String>();
		fieldsInQuery.addAll(fieldsSelect);
		fieldsInQuery.addAll(fieldsInWhere);
		Set<Integer> fieldsId=new HashSet<Integer>();
		 Map<String,Integer> hashTableColAndId=Configuration.getHashTableColAndId();
		for(int i=0;i<fieldsInQuery.size();i++){
			 fieldsId.add(hashTableColAndId.get(fieldsInQuery.get(i)));
		}
		 Map<String,Integer> hashPadAndCount=Configuration.getHashPadAndCount();
		 Multimap<String,Integer> hashPadAndId=Configuration.getHashPadAndId();
		 Multimap<String,String> hashDateAndPad=Configuration.getHashDateAndPad();
		 Collection<String> padListOfDate=hashDateAndPad.get(dateCol);
		 
		 Iterator<String> iterate= padListOfDate.iterator();
		 long totalCount=Long.MAX_VALUE;
		 while(iterate.hasNext()){
		    String padId=iterate.next();
		    Collection<Integer> value=hashPadAndId.get(padId);
		    if(value.containsAll(fieldsId)){
		    	int padcount=hashPadAndCount.get(padId);
		    	if(padcount<totalCount){
		    	  totalCount=padcount;
		    	  bestPad=padId;
		    	}
		    }
		 }
		return bestPad;
	}
}
