package changeQuery;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import beans.TableAlias;

public class ParseUtil {

	public  Set<String> getSelectList(String[] arrSelect, String balfield, String acctDt, Map<String, String> allTableAndAlias) {
		 Set<String> listSelect=new HashSet<String>();
		 String field="";
		 for(int i=0;i<arrSelect.length;i++){
			 if(arrSelect[i].indexOf("as")>0)
				  field=arrSelect[i].substring(arrSelect[i].indexOf(".")+1, arrSelect[i].indexOf("as")).trim(); 
			 else
		          field=arrSelect[i].substring(arrSelect[i].indexOf(".")+1, arrSelect[i].length()).trim();
			 if(!field.contains(balfield) && !field.equals(acctDt)){
				 String alias=arrSelect[i].substring(0,arrSelect[i].indexOf(".")).trim();
				 String tableName=allTableAndAlias.get(alias);
				 if(tableName.equals(Configuration.getCalculationTable()))
					 tableName=Configuration.getCalculationOnTable();
				 
				 listSelect.add(tableName+"."+field);
			 }
		 }	 
		 return listSelect;
	}
	public  Set<String> getWhereList(String[] splitWhere, String calculatedcolumn, String txndate, Map<String, String> allTableAndAlias) {
		// TODO Auto-generated method stub
		Set<String> listSelect=new HashSet<String>();
		 String field="";
		 String fieldInWhere="";
		 for(int i=0;i<splitWhere.length;i++){
			 field=splitWhere[i].trim();
			 Boolean isJoin=false;
			 if(field.indexOf("=")>0){
				 String secondCol=field.substring(field.indexOf("=")+1);
				 if(secondCol.indexOf(".")>0){
					 String join=secondCol.substring(0,secondCol.indexOf(".")).trim();
					 allTableAndAlias.containsKey(join);
					 isJoin=true;
				 }
					 
			 }
			 if(!isJoin){
			 field=field.substring(0, field.indexOf(" "));
			 fieldInWhere=field.substring(field.indexOf(".")+1, field.length()).trim();
			 
			 if(!txndate.equals(fieldInWhere.trim())){
				 String alias=field.substring(0,field.indexOf(".")).trim();
				 String tableName=allTableAndAlias.get(alias);
				 if(tableName.equals(Configuration.getCalculationTable()))
					 tableName=Configuration.getCalculationOnTable();
				   listSelect.add(tableName+"."+fieldInWhere);
			 }
			 }
		 }
		 return listSelect;
	}

	public  String createSetToCommaString(Set<String> fieldsSelect) {
		// TODO Auto-generated method stub
		String allField="";
		Set<String> filedSet=new HashSet();
		for(String f:fieldsSelect){
			filedSet.add(f.substring(f.indexOf(".")+1, f.length()));
		}
		allField=StringUtils.join(filedSet, ',');
 		return allField;
	}
		public  String getWhereCondition(String[] splitWhere, String calculatedCol, String acctDateColumn, List<TableAlias> listCombindTableAlias, String tableAlias) {
		// TODO Auto-generated method stub
		String whereCon="";
		 String field1="";
		 for(int i=0;i<splitWhere.length;i++){
			 boolean isExits=false;
			 field1=splitWhere[i].trim();
			  field1=field1.substring(field1.indexOf(".")+1, field1.indexOf(" "));
			   if(listCombindTableAlias!=null &&listCombindTableAlias.size()>0 && splitWhere[i].indexOf("=")>0){
				   String field2=splitWhere[i].trim().split("=")[1];
				   
				    if(field2.indexOf(".")<0)
				    isExits=false;
				    else{
				    field2=field2.substring(0,field2.indexOf(".")).trim().toString();
				       for(int j=0;j<listCombindTableAlias.size();j++){
				    	   if(listCombindTableAlias.get(j).getAlias().equals(field2) || tableAlias.equals(field2))
				    	   isExits=true;
				    	   break;
				       }
				       
				   }
			   }
			    if(!field1.equals(acctDateColumn) && !isExits){
			    	String value=splitWhere[i];
			    	whereCon+=value.replaceFirst(value.substring(0, value.indexOf(".")+1), "")+" and ";
			    }
			    
		 } 
		 if(whereCon!="")
		 return whereCon.substring(0, whereCon.length()-5);
		 return whereCon;
	}	
}
