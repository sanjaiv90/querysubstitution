package org.svayam.jdbc;

import java.sql.SQLException;
import java.util.concurrent.locks.ReentrantLock;

import org.mariadb.jdbc.MariaDbConnection;
import org.mariadb.jdbc.internal.protocol.Protocol;

public class SvayamMariaDbConnection extends MariaDbConnection {

	SvayamMariaDbConnection(Protocol protocol, ReentrantLock lock) throws SQLException{
		super(protocol,lock);
	}
}
