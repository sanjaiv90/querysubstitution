package org.svayam.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;

import org.apache.calcite.sql.parser.SqlParseException;
import org.apache.log4j.Logger;
import org.mariadb.jdbc.MariaDbConnection;
import org.mariadb.jdbc.MariaDbStatement;
import org.mariadb.jdbc.internal.queryresults.resultset.MariaSelectResultSet;

import changeQuery.Configuration;
import changeQuery.SetDriverMetaDataConfig;
import changeQuery.SvayamParserSqlQuery;

public class SvayamMariaDbStatement extends MariaDbStatement {
	static MariaDbConnection myCon;
	final static Logger logger = Logger.getLogger(SvayamMariaDbStatement.class);
	public SvayamMariaDbStatement(MariaDbConnection connection, int resultSetScrollType) {
		super(connection, resultSetScrollType);
		
		
		setConnection(super.connection);
	}
	private void setConnection(MariaDbConnection mariaDbConnection) {
		// TODO Auto-generated method stub
		myCon=mariaDbConnection;
		
	}
	public static  MariaDbConnection getCon(){
	return	myCon;
		
	}
	/**
     * executes a select query.
     *
     * @param sql the query to send to the server
     * @return a result set
     * @throws SQLException if something went wrong
     */
    public ResultSet executeQuery(String sql) throws SQLException {
    	//add by sanjai
    
    	
    	/*if(sql.contains("svayam_dev.transaction")){
    		sql="select t.*,0 as bal from svayam_dev.transaction t";
    		
    	}*/
    	logger.info("Sql QueryExecute: "+sql);
    	/*Boolean isCalculatedColumn=false;
		isCalculatedColumn = SvayamParserSqlQuery.IsChangeQuery(sql);
		
		if(isCalculatedColumn){
		 try {
			 sql=SvayamParserSqlQuery.changeQuery(sql);
		} catch (SqlParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
    	*/
    	
		if (executeInternal(sql, fetchSize)) {
            return results.getResultSet();
        }
        //throw new SQLException("executeQuery() with query '" + query +"' did not return a result set");
        return MariaSelectResultSet.EMPTY;
    }
}
