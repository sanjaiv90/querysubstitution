package org.svayam.jdbc;

import java.io.IOException;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.List;

import org.mariadb.jdbc.MariaDbResultSetMetaData;
import org.mariadb.jdbc.internal.packet.dao.ColumnInformation;
import org.mariadb.jdbc.internal.packet.read.ReadPacketFetcher;
import org.mariadb.jdbc.internal.protocol.Protocol;
import org.mariadb.jdbc.internal.queryresults.Results;
import org.mariadb.jdbc.internal.queryresults.resultset.MariaSelectResultSet;
import org.mariadb.jdbc.internal.util.dao.QueryException;

public class SvayamMariaSelectResultSet extends MariaSelectResultSet{

	public SvayamMariaSelectResultSet(ColumnInformation[] columnInformation, Results results, Protocol protocol,
			ReadPacketFetcher fetcher, boolean callableResult) throws IOException, QueryException {
		     super(columnInformation, results, protocol, fetcher, callableResult);
		
	}
	 public ResultSetMetaData getMetaData() throws SQLException {
	        return new MariaDbResultSetMetaData(columnsInformation, dataTypeMappingFlags, returnTableAlias);
	    }
	

}
