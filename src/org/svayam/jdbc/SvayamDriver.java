package org.svayam.jdbc;

import java.sql.DriverManager;
import java.sql.SQLException;

import org.mariadb.jdbc.Driver;

public class SvayamDriver extends Driver {
	
	 static {
	        try {
	            DriverManager.registerDriver(new Driver());
	        } catch (SQLException e) {
	            throw new RuntimeException("Could not register driver", e);
	        }
	    }

}
