package org.svayam.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;
import org.mariadb.jdbc.MariaDbDatabaseMetaData;
import org.mariadb.jdbc.internal.MariaDbType;
import org.mariadb.jdbc.internal.packet.dao.ColumnInformation;
import org.mariadb.jdbc.internal.queryresults.resultset.MariaSelectResultSet;

import changeQuery.Configuration;
import changeQuery.SetDriverMetaDataConfig;
import metadata.ConnectToSvayamDev;

public class SvayamMariaDbDatabaseMetaData extends MariaDbDatabaseMetaData {
	final static Logger logger = Logger.getLogger(SvayamMariaDbDatabaseMetaData.class);
	public SvayamMariaDbDatabaseMetaData(Connection connection, String user, String url) {
		super(connection, user, url);
		
	}
	 public ResultSet getCatalogs() throws SQLException {
		 
	        return executeSvayamDbQuery(
	                "SELECT SCHEMA_NAME TABLE_CAT FROM INFORMATION_SCHEMA.SCHEMATA ORDER BY 1");
	    }
 public ResultSet executeSvayamDbQuery(String sql) throws SQLException {
	 logger.info("Db Query"+sql);
        MariaSelectResultSet rs = (MariaSelectResultSet) connection.createStatement().executeQuery(sql);
        rs.setStatement(null); // bypass Hibernate statement tracking (CONJ-49)
        rs.setReturnTableAlias(true);
        
        //Code Added by sanjai
        	 List<String> databaseToHideAsList=ConnectToSvayamDev.getConfigDbMatadata();
        	 //configdb table contains data then return svayamResultSet
        	 if(databaseToHideAsList!=null && databaseToHideAsList.size()>0){
        		
        ColumnInformation[] columnInfo = new ColumnInformation[1];
        columnInfo[0] = ColumnInformation.create("insert_id", MariaDbType.STRING);
        int rows=0;
        if (rs.last()) {
        	  rows = rs.getRow();
         	  rs.beforeFirst();
        	}
        int i=0;
        String[] columnName={"TABLE_CAT"};
        MariaDbType[] columnTypes=new MariaDbType[1];
        columnTypes[0]=MariaDbType.STRING;
		String[][] arrData=new String[rows][1];
        		 while(rs.next()){
        			 
        			if(!databaseToHideAsList.contains(rs.getString("TABLE_CAT"))){
        				arrData[i][0]=rs.getString("TABLE_CAT");
        				i++;
        			}
        		}
        		 ResultSet svayamResultSet=MariaSelectResultSet.createResultSet(columnName,columnTypes,arrData, connection.getProtocol());
        		 rs.beforeFirst(); 
        		 return svayamResultSet;
        	 }
        	 // configdb do not contain data then return original ResultSet
        	 return rs;
    }
 /**
  * Retrieves a description of the tables available in the given catalog. Only table descriptions matching the catalog, schema, table name and type
  * criteria are returned.  They are ordered by <code>TABLE_TYPE</code>, <code>TABLE_CAT</code>, <code>TABLE_SCHEM</code> and
  * <code>TABLE_NAME</code>.
  * Each table description has the following columns: <OL> <LI><B>TABLE_CAT</B> String {@code =>} table catalog (may be <code>null</code>)
  * <LI><B>TABLE_SCHEM</B> String {@code =>} table schema (may be <code>null</code>) <LI><B>TABLE_NAME</B> String {@code =>} table name
  * <LI><B>TABLE_TYPE</B> String {@code =>} table type.  Typical types are "TABLE", "VIEW", "SYSTEM TABLE", "GLOBAL TEMPORARY", "LOCAL TEMPORARY",
  * "ALIAS", "SYNONYM". <LI><B>REMARKS</B> String {@code =>} explanatory comment on the table <LI><B>TYPE_CAT</B> String {@code =>} the types
  * catalog (may be <code>null</code>) <LI><B>TYPE_SCHEM</B> String {@code =>} the types schema (may be <code>null</code>) <LI><B>TYPE_NAME</B>
  * String {@code =>} type name (may be <code>null</code>) <LI><B>SELF_REFERENCING_COL_NAME</B> String {@code =>} name of the designated
  * "identifier" column of a typed table (may be <code>null</code>) <LI><B>REF_GENERATION</B> String {@code =>} specifies how values in
  * SELF_REFERENCING_COL_NAME are created. Values are "SYSTEM", "USER", "DERIVED". (may be <code>null</code>) </OL>
  * <P><B>Note:</B> Some databases may not return information for all tables.
  *
  * @param catalog a catalog name; must match the catalog name as it is stored in the database; "" retrieves those without a catalog;
  * <code>null</code> means that the catalog name should not be used to narrow the search
  * @param schemaPattern a schema name pattern; must match the schema name as it is stored in the database; "" retrieves those without a schema;
  * <code>null</code> means that the schema name should not be used to narrow the search
  * @param tableNamePattern a table name pattern; must match the table name as it is stored in the database
  * @param types a list of table types, which must be from the list of table types returned from {@link #getTableTypes},to include;
  * <code>null</code> returns all types
  * @return <code>ResultSet</code> - each row is a table description
  * @throws SQLException if a database access error occurs
  * @see #getSearchStringEscape
  */
 public ResultSet getTables(String catalog, String schemaPattern, String tableNamePattern, String[] types)
         throws SQLException {

     String sql =
             "SELECT TABLE_SCHEMA TABLE_CAT, NULL  TABLE_SCHEM,  TABLE_NAME, IF(TABLE_TYPE='BASE TABLE', 'TABLE', TABLE_TYPE) as TABLE_TYPE,"
                     + " TABLE_COMMENT REMARKS, NULL TYPE_CAT, NULL TYPE_SCHEM, NULL TYPE_NAME, NULL SELF_REFERENCING_COL_NAME, "
                     + " NULL REF_GENERATION"
                     + " FROM INFORMATION_SCHEMA.TABLES "
                     + " WHERE "
                     + catalogCond("TABLE_SCHEMA", catalog)
                     + " AND "
                     + patternCond("TABLE_NAME", tableNamePattern);

     if (types != null && types.length > 0) {
         sql += " AND TABLE_TYPE IN (";
         for (int i = 0; i < types.length; i++) {
             if (types[i] == null) {
                 continue;
             }
             String type = escapeQuote(mapTableTypes(types[i]));
             if (i == types.length - 1) {
                 sql += type + ")";
             } else {
                 sql += type + ",";
             }
         }
     }

     sql += " ORDER BY TABLE_TYPE, TABLE_SCHEMA, TABLE_NAME";

     return executeSvayamTableQuery(sql);
 }
 private ResultSet executeSvayamTableQuery(String sql) throws SQLException {
	 logger.info("Table Query"+sql);
     MariaSelectResultSet rs = (MariaSelectResultSet) connection.createStatement().executeQuery(sql);
     rs.setStatement(null); // bypass Hibernate statement tracking (CONJ-49)
     rs.setReturnTableAlias(true);
   //Code Added by sanjai
	 String catalogName=null;
	 //configdb table contains data then return svayamResultSet
	   int rows=0;
	   /*  String catalogName=null;*/
	     if (rs.last()) {
	     	  rows = rs.getRow();
	     	 catalogName=rs.getString(1);
	      	  rs.beforeFirst();
	     	}
	     
		 //configdb table contains data then return svayamResultSet
<<<<<<< HEAD
	 if(catalogName !=null && catalogName.equals("svayam_data") /*databaseToHideAsList!=null && databaseToHideAsList.size()>0*/){
=======
	 if(catalogName !=null && Configuration.getPadDataCatalog()!=null && catalogName.equals(Configuration.getPadDataCatalog()) /*databaseToHideAsList!=null && databaseToHideAsList.size()>0*/){
>>>>>>> 96e1190e0d92e774a48885b593e03964bcfb8a26
			
	ColumnInformation[] columnInfo = new ColumnInformation[1];
	columnInfo[0] = ColumnInformation.create("insert_id", MariaDbType.STRING);

	ResultSetMetaData rsmd=rs.getMetaData();
	int totalcolumn=rsmd.getColumnCount();
	String[] columnName=new String[totalcolumn];
	MariaDbType[] columnTypes=new MariaDbType[totalcolumn];

	for(int col=0;col<totalcolumn;col++){
		columnName[col]=rsmd.getColumnName(col+1);
		
		if(rsmd.getColumnTypeName(col+1).contains("VARCHAR"))
			columnTypes[col]=MariaDbType.VARCHAR;
	else if(rsmd.getColumnTypeName(col+1).contains("BIGINT"))
			columnTypes[col]=MariaDbType.BIGINT;
		else
			columnTypes[col]=MariaDbType.NULL;
	}

	int i=0;
	//MariaDbType[] columnTypes=new MariaDbType[1];

	String[][] arrData=new String[rows+1][totalcolumn];

			 while(rs.next()){
				 for(int col=0;col<totalcolumn;col++)
					arrData[i][col]=rs.getString(col+1);
					i++;	 
			}
			 //add new column
			
//		 0 TABLE_CAT, 1 TABLE_SCHEM, 2 TABLE_NAME, 3 COLUMN_NAME, 4 DATA_TYPE, 5 TYPE_NAME, 6 COLUMN_SIZE, 7 BUFFER_LENGTH,  8 DECIMAL_DIGITS,
	//  9 NUM_PREC_RADIX, 10 NULLABLE, 11 REMARKS,12 COLUMN_DEF,13 SQL_DATA_TYPE, 14 SQL_DATETIME_SUB,15 CHAR_OCTET_LENGTH, 16 ORDINAL_POSITION, 
//		17 IS_NULLABLE, 18 SCOPE_CATALOG, 19 SCOPE_SCHEMA, 20 SCOPE_TABLE,21 SOURCE_DATA_TYPE, 22 IS_AUTOINCREMENT,23 IS_GENERATEDCOLUMN
<<<<<<< HEAD
			 arrData[i][0]="svayam_data";
=======
			 arrData[i][0]=catalogName;
>>>>>>> 96e1190e0d92e774a48885b593e03964bcfb8a26
			 arrData[i][1]="null";
			 arrData[i][2]="transaction_bal";
			 arrData[i][3]="table";
			 arrData[i][4]="";
			 arrData[i][5]="";
			 arrData[i][6]="";
			 arrData[i][7]="";
			 arrData[i][8]="";
			 arrData[i][9]="";
			
			 
			 ResultSet svayamResultSet=MariaSelectResultSet.createResultSet(columnName,columnTypes,arrData, connection.getProtocol());
			 rs.beforeFirst(); 
			 return svayamResultSet;
		 }
	     
	     return rs;
 }
 /**
  * Retrieves a description of table columns available in the specified catalog.
  * <P>Only column descriptions matching the catalog, schema, table and column name criteria are returned.  They are ordered by
  * <code>TABLE_CAT</code>,<code>TABLE_SCHEM</code>, <code>TABLE_NAME</code>, and <code>ORDINAL_POSITION</code>.
  * <P>Each column description has the following columns: <OL> <LI><B>TABLE_CAT</B> String {@code =>} table catalog (may be <code>null</code>)
  * <LI><B>TABLE_SCHEM</B> String {@code =>} table schema (may be <code>null</code>) <LI><B>TABLE_NAME</B> String {@code =>} table name
  * <LI><B>COLUMN_NAME</B> String {@code =>} column name <LI><B>DATA_TYPE</B> int {@code =>} SQL type from java.sql.Types <LI><B>TYPE_NAME</B>
  * String {@code =>} Data source dependent type name, for a UDT the type name is fully qualified <LI><B>COLUMN_SIZE</B> int {@code =>} column
  * size. <LI><B>BUFFER_LENGTH</B> is not used. <LI><B>DECIMAL_DIGITS</B> int {@code =>} the number of fractional digits. Null is returned for data
  * types where DECIMAL_DIGITS is not applicable. <LI><B>NUM_PREC_RADIX</B> int {@code =>} Radix (typically either 10 or 2) <LI><B>NULLABLE</B> int
  * {@code =>} is NULL allowed. <UL> <LI> columnNoNulls - might not allow <code>NULL</code> values <LI> columnNullable - definitely allows
  * <code>NULL</code> values <LI> columnNullableUnknown - nullability unknown </UL> <LI><B>REMARKS</B> String {@code =>} comment describing column
  * (may be <code>null</code>) <LI><B>COLUMN_DEF</B> String {@code =>} default value for the column, which should be interpreted as a string when
  * the value is enclosed in single quotes (may be <code>null</code>) <LI><B>SQL_DATA_TYPE</B> int {@code =>} unused <LI><B>SQL_DATETIME_SUB</B>
  * int {@code =>} unused <LI><B>CHAR_OCTET_LENGTH</B> int {@code =>} for char types the maximum number of bytes in the column
  * <LI><B>ORDINAL_POSITION</B> int {@code =>} index of column in table (starting at 1) <LI><B>IS_NULLABLE</B> String  {@code =>} ISO rules are
  * used to determine the nullability for a column. <UL> <LI> YES           --- if the column can include NULLs <LI> NO            --- if the
  * column cannot include NULLs <LI> empty string  --- if the nullability for the column is unknown </UL> <LI><B>SCOPE_CATALOG</B> String {@code
  * =>} catalog of table that is the scope of a reference attribute (<code>null</code> if DATA_TYPE isn't REF) <LI><B>SCOPE_SCHEMA</B> String
  * {@code =>} schema of table that is the scope of a reference attribute (<code>null</code> if the DATA_TYPE isn't REF) <LI><B>SCOPE_TABLE</B>
  * String {@code =>} table name that this the scope of a reference attribute (<code>null</code> if the DATA_TYPE isn't REF)
  * <LI><B>SOURCE_DATA_TYPE</B> short {@code =>} source type of a distinct type or user-generated Ref type, SQL type from java.sql.Types
  * (<code>null</code> if DATA_TYPE isn't DISTINCT or user-generated REF) <LI><B>IS_AUTOINCREMENT</B> String  {@code =>} Indicates whether this
  * column is auto incremented <UL> <LI> YES           --- if the column is auto incremented <LI> NO            --- if the column is not auto
  * incremented <LI> empty string  --- if it cannot be determined whether the column is auto incremented </UL> <LI><B>IS_GENERATEDCOLUMN</B> String
  * {@code =>} Indicates whether this is a generated column <UL> <LI> YES           --- if this a generated column <LI> NO            --- if this
  * not a generated column <LI> empty string  --- if it cannot be determined whether this is a generated column </UL> </OL>
  * <p>The COLUMN_SIZE column specifies the column size for the given column. For numeric data, this is the maximum precision.  For character data,
  * this is the length in characters. For datetime datatypes, this is the length in characters of the String representation (assuming the maximum
  * allowed precision of the fractional seconds component). For binary data, this is the length in bytes.  For the ROWID datatype, this is the
  * length in bytes. Null is returned for data types where the column size is not applicable.
  *
  * @param catalog a catalog name; must match the catalog name as it is stored in the database; "" retrieves those without a catalog;
  * <code>null</code> means that the catalog name should not be used to narrow the search
  * @param schemaPattern a schema name pattern; must match the schema name as it is stored in the database; "" retrieves those without a schema;
  * <code>null</code> means that the schema name should not be used to narrow the search
  * @param tableNamePattern a table name pattern; must match the table name as it is stored in the database
  * @param columnNamePattern a column name pattern; must match the column name as it is stored in the database
  * @return <code>ResultSet</code> - each row is a column description
  * @throws SQLException if a database access error occurs
  * @see #getSearchStringEscape
  */
 public ResultSet getColumns(String catalog, String schemaPattern, String tableNamePattern, String columnNamePattern)
         throws SQLException {
	 boolean isChangeConfig=false;
	   if(tableNamePattern.equals("transaction_bal")){
		   tableNamePattern="transaction";
		   isChangeConfig=true;
	   }
	   
     String sql = "SELECT TABLE_SCHEMA TABLE_CAT, NULL TABLE_SCHEM, TABLE_NAME, COLUMN_NAME,"
             + dataTypeClause("COLUMN_TYPE") + " DATA_TYPE,"
             + columnTypeClause("COLUMN_TYPE") + " TYPE_NAME, "
             + " CASE DATA_TYPE"
             + "  WHEN 'time' THEN "
                 + (datePrecisionColumnExist ? "IF(DATETIME_PRECISION = 0, 10, CAST(11 + DATETIME_PRECISION as signed integer))" : "10")
             + "  WHEN 'date' THEN 10"
             + "  WHEN 'datetime' THEN "
                 + (datePrecisionColumnExist ? "IF(DATETIME_PRECISION = 0, 19, CAST(20 + DATETIME_PRECISION as signed integer))" : "19")
             + "  WHEN 'timestamp' THEN "
                 + (datePrecisionColumnExist ? "IF(DATETIME_PRECISION = 0, 19, CAST(20 + DATETIME_PRECISION as signed integer))" : "19")
             + "  ELSE "
             + "  IF(NUMERIC_PRECISION IS NULL, LEAST(CHARACTER_MAXIMUM_LENGTH," + Integer.MAX_VALUE + "), NUMERIC_PRECISION) "
             + " END"
             + " COLUMN_SIZE, 65535 BUFFER_LENGTH, NUMERIC_SCALE DECIMAL_DIGITS,"
             + " 10 NUM_PREC_RADIX, IF(IS_NULLABLE = 'yes',1,0) NULLABLE,COLUMN_COMMENT REMARKS,"
             + " COLUMN_DEFAULT COLUMN_DEF, 0 SQL_DATA_TYPE, 0 SQL_DATETIME_SUB,  "
             + " LEAST(CHARACTER_OCTET_LENGTH," + Integer.MAX_VALUE + ") CHAR_OCTET_LENGTH,"
             + " ORDINAL_POSITION, IS_NULLABLE, NULL SCOPE_CATALOG, NULL SCOPE_SCHEMA, NULL SCOPE_TABLE, NULL SOURCE_DATA_TYPE,"
             + " IF(EXTRA = 'auto_increment','YES','NO') IS_AUTOINCREMENT, "
             + " IF(EXTRA in ('VIRTUAL', 'PERSISTENT', 'VIRTUAL GENERATED', 'STORED GENERATED') ,'YES','NO') IS_GENERATEDCOLUMN "
             + " FROM INFORMATION_SCHEMA.COLUMNS  WHERE "
             + catalogCond("TABLE_SCHEMA", catalog)
             + " AND "
             + patternCond("TABLE_NAME", tableNamePattern)
             + " AND "
             + patternCond("COLUMN_NAME", columnNamePattern)
             + " ORDER BY TABLE_CAT, TABLE_SCHEM, TABLE_NAME, ORDINAL_POSITION";

     try {
         return executeSvayamColumnQuery(sql,isChangeConfig);
     } catch (SQLException sqlException) {
         if (sqlException.getMessage().contains("Unknown column 'DATETIME_PRECISION'")) {
             datePrecisionColumnExist = false;
             return getColumns(catalog, schemaPattern, tableNamePattern, columnNamePattern);
         }
         throw sqlException;
     }
 }
 public ResultSet executeSvayamColumnQuery(String sql,boolean isChangeConfig) throws SQLException {
	 logger.info("Column Query"+sql);
     MariaSelectResultSet rs = (MariaSelectResultSet) connection.createStatement().executeQuery(sql);
     rs.setStatement(null); // bypass Hibernate statement tracking (CONJ-49)
     rs.setReturnTableAlias(true);
     

     int rows=0;
     String tableName=null;
     if (rs.last()) {
     	  rows = rs.getRow();
     	  tableName=rs.getString(3);
      	  rs.beforeFirst();
     	}
     
	 //configdb table contains data then return svayamResultSet
 if(isChangeConfig/*tableName !=null && tableName.equals("transaction") databaseToHideAsList!=null && databaseToHideAsList.size()>0*/){
		
ColumnInformation[] columnInfo = new ColumnInformation[1];
columnInfo[0] = ColumnInformation.create("insert_id", MariaDbType.STRING);

ResultSetMetaData rsmd=rs.getMetaData();

int totalcolumn=rsmd.getColumnCount();
String[] columnName=new String[totalcolumn];
MariaDbType[] columnTypes=new MariaDbType[totalcolumn];

for(int col=0;col<totalcolumn;col++){
	columnName[col]=rsmd.getColumnName(col+1);
	
	if(rsmd.getColumnTypeName(col+1).contains("VARCHAR"))
		columnTypes[col]=MariaDbType.VARCHAR;
else if(rsmd.getColumnTypeName(col+1).contains("BIGINT"))
		columnTypes[col]=MariaDbType.BIGINT;
	else
		columnTypes[col]=MariaDbType.NULL;
}

int i=0;
//total t

String[][] arrData=new String[rows-3][totalcolumn];

		 while(rs.next()){
			 if(!rs.getString("column_name").equals("txn_amt")&& !rs.getString("column_name").equals("txn_id")
					 && !rs.getString("column_name").equals("proc_dt") && !rs.getString("column_name").equals("wrk_dt")
					 ){
			 for(int col=0;col<totalcolumn;col++)
				arrData[i][col]=rs.getString(col+1);
				i++;	
				 }
		}
		 //add new column
		
//	 0 TABLE_CAT, 1 TABLE_SCHEM, 2 TABLE_NAME, 3 COLUMN_NAME, 4 DATA_TYPE, 5 TYPE_NAME, 6 COLUMN_SIZE, 7 BUFFER_LENGTH,  8 DECIMAL_DIGITS,
//  9 NUM_PREC_RADIX, 10 NULLABLE, 11 REMARKS,12 COLUMN_DEF,13 SQL_DATA_TYPE, 14 SQL_DATETIME_SUB,15 CHAR_OCTET_LENGTH, 16 ORDINAL_POSITION, 
//	17 IS_NULLABLE, 18 SCOPE_CATALOG, 19 SCOPE_SCHEMA, 20 SCOPE_TABLE,21 SOURCE_DATA_TYPE, 22 IS_AUTOINCREMENT,23 IS_GENERATEDCOLUMN
		 arrData[i][3]="balance";
		 arrData[i][4]="3";
		 arrData[i][5]="DECIMAL";
		 arrData[i][6]="10";
		 arrData[i][7]="65535";
		 arrData[i][8]="0";
		 arrData[i][9]="10";
		 arrData[i][10]="1";
		 arrData[i][11]="";
		 arrData[i][13]="0";
		 arrData[i][14]="0";
		 arrData[i][16]="32";
		 arrData[i][17]="YES";
		 
		 ResultSet svayamResultSet=MariaSelectResultSet.createResultSet(columnName,columnTypes,arrData, connection.getProtocol());
		 rs.beforeFirst(); 
		 return svayamResultSet;
	 }
     
     return rs;
 }
}
