package org.svayam.jdbc;

import org.mariadb.jdbc.MariaDbResultSetMetaData;
import org.mariadb.jdbc.internal.packet.dao.ColumnInformation;

public class SvayamMariaDbResultSetMetaData extends MariaDbResultSetMetaData{

	public SvayamMariaDbResultSetMetaData(ColumnInformation[] fieldPackets, int datatypeMappingFlags,
			boolean returnTableAlias) {
		super(fieldPackets, datatypeMappingFlags, returnTableAlias);
	}

}
