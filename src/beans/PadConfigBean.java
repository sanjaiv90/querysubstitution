package beans;

import java.util.List;

public class PadConfigBean {

	private String padId;
	private List<String> padCol;
	private int padCount;
	public String getPadId() {
		return padId;
	}
	public void setPadId(String padId) {
		this.padId = padId;
	}
	public List<String> getPadCol() {
		return padCol;
	}
	public void setPadCol(List<String> padCol) {
		this.padCol = padCol;
	}
	public int getPadCount() {
		return padCount;
	}
	public void setPadCount(int padCount) {
		this.padCount = padCount;
	}
	
}
